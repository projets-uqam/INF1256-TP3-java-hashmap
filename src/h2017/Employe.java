package h2017;

public class Employe extends Travailleur {

    final static int NOMBRE_MINUTES_PAR_HEURE = 60;

    // constructeur
    public Employe(String ligne) {
        decomposerFeuilleTemps(ligne);
    }

    //matricule
    @Override
    void setMatricule(int matricule) {
        super.setMatricule(matricule);
    }

    @Override
    public int getMatricule() {
        return super.getMatricule();
    }

    //nom
    @Override
    void setNom(String nomTrav) {
        super.setNom(nomTrav);
    }

    @Override
    public String getNom() {
        return super.getNom();
    }

    //prenom
    @Override
    void setPrenom(String prenomTrav) {
        super.setPrenom(prenomTrav);
    }

    @Override
    public String getPrenom() {
        return super.getPrenom();
    }

    //nhBanque
    @Override
    void setNhBanque(double nhBank) {
        super.setNhBanque(nhBank);
    }

    @Override
    public double getNhBanque() {
        return super.getNhBanque();
    }

    //nhTravaillees
    @Override
    void setNhTravaillees(double nhTrav) {
        super.setNhTravaillees(nhTrav);
    }

    @Override
    public double getNhTravaillees() {
        return super.getNhTravaillees();
    }

    //nhReprises
    @Override
    void setNhReprises(double nhRep) {
        super.setNhReprises(nhRep);
    }

    @Override
    public double getNhReprises() {
        return super.getNhReprises();
    }

    //nhReprises
    @Override
    void setStatut(StatutFeuille stat) {
        super.setStatut(stat);
    }

    @Override
    public StatutFeuille getStatut() {
        return super.getStatut();
    }

    //commentaire
    @Override
    void setCommentaire(String comment) {
        super.setCommentaire(comment);
    }

    @Override
    public String getCommentaire() {
        return super.getCommentaire();
    }

    //---------implementation des méthodes abstraites ------------- //

    //décomposition des élements a partir d'une ligne de texte
    @Override
    final void decomposerFeuilleTemps(String ligneFeuilleTemps) {
        String[] dataLigne = ligneFeuilleTemps.split("\\|");
        setMatricule(Integer.parseInt(dataLigne[0]));
        setNom(dataLigne[1]);
        setPrenom(dataLigne[2]);
        setNhBanque(Double.parseDouble(dataLigne[3]));
        setNhTravaillees(Double.parseDouble(dataLigne[4]));
        setNhReprises(Double.parseDouble(dataLigne[5]));
        if (dataLigne.length > 6) {
            switch (dataLigne[6]) {
                case "ACC":
                    setStatut(STATUT_ACCEPTE);
                    break;
                case "REJ":
                    setStatut(STATUT_REJETE);
                    break;
            }
        }
        if (dataLigne.length > 7) {
            setCommentaire(dataLigne[7]);
        }
    }

    //rassembler elements pour former une ligne de texte.
    @Override
    public String assemblerFeuilleTemps() {
        String dataLigne;
        dataLigne = getMatricule() + "|" + getNom() + "|" + getPrenom() + "|" + getNhBanque() + "|" + getNhTravaillees() + "|" + getNhReprises();
        if (getStatut() != null) {
            dataLigne = dataLigne + "|" + getStatut();
        }
        if (getCommentaire() != null && !"".equals(getCommentaire())) {
            dataLigne = dataLigne + "|" + getCommentaire();
        }
        dataLigne = dataLigne + "\n";
        return dataLigne;
    }

    //affichage d'une donnée de heures
    @Override
    void afficheHeures(String message, double nbheures) {
        double heuresEntieres;
        double minutes;
        heuresEntieres = Math.floor(nbheures);
        minutes = (nbheures - heuresEntieres) * NOMBRE_MINUTES_PAR_HEURE;
        System.out.format(message + "%.0f:%02d %n", heuresEntieres, Math.round(minutes));
    }

    //la methode suivante affiche les information de la feuille de temps
    //une information par ligne
    @Override
    public void afficheFeuilleTemps() {
        // Affichage de la feuille de temps
        System.out.println("\n---------------------------------------");
        System.out.format("Feuille de Temps de l'employé No. %d  %n", getMatricule());
        System.out.print("---------------------------------------\n");
        System.out.format("Nom: %s %n", getNom());
        System.out.format("Prénom: %s %n", getPrenom());
        afficheHeures("Nombre d'heures en banque: ", getNhBanque());
        afficheHeures("Nombre d'heures travaillées: ", getNhTravaillees());
        afficheHeures("Nombre d'heures reprises: ", getNhReprises());
        if (getStatut() != null) {
            System.out.format("Statut: %s %n", getStatut());
        }
        if (getCommentaire() != null && !"".equals(getCommentaire())) {
            System.out.format("Commentaire: %s %n", getCommentaire());
        }
        System.out.print("---------------------------------------\n");
    }
}
