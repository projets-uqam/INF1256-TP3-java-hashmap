package h2017;

import java.io.*;
import java.nio.file.*;
import java.util.*;


public class ProgrammePrincipal {

    // declaration des constantes
    private static final String FICHIER_SOURCE = "feuillesTemps";
    private static final String FICHIER_SORTIE = "feuillesValidees";
    private static final String LIGNE = "\n-------------------------------------------------------------------------\n";
    private static final String ATTENTION_ONLY_NOMBRES = "\nATTENTION! des \"nombres entiers\" seulement acceptées\n ";
    private static final String ATTENTION_CHOIX_INVALIDE = "\nATTENTION!, vous avez entré un choix invalide\n";
    private static final String ATTENTION_LETTRES = "ATTENTION! des \"lettres\" seulement accepté";
    // private static final String DELIMITEUR_SAISIE = System.lineSeparator(); // pour Windows Terminal
    final static String DELIMITEUR_SAISIE = "\n"; // pour Linux Terminal et IDE NetBeans 8.0.2
    private static final String PATTERN_COMMENT = "[a-zA-Z]{1,}.{0,}";

    //déclaration du scanner  
    Scanner clavier;

    public static void main(String[] args) {

        ProgrammePrincipal tp3 = new ProgrammePrincipal();

        // init mon input from user
        tp3.clavier = new Scanner(System.in).useDelimiter(DELIMITEUR_SAISIE);

        // collection des Employes
        Map<Integer, Travailleur> bd = new HashMap<>();

        // on charge les feuilles des temps
        tp3.lireFeuillesTemps(FICHIER_SOURCE, bd);

        // presentation du programme
        tp3.bienvenue();

        // exécution du programme
        tp3.afficherMenu(bd);
    }

    
    // affiche la présentation du programme 
    void bienvenue() {
        System.out.println("Bienvenue au programme: Gestion des Feuilles de Temps" + LIGNE);
        System.out.println("- Ce programme sert à établir les feuilles de temps des employés \nde la societé QCRH Inc\n ");
        System.out.println("- L’application affiche, valide et sauvegarde des données des Feuilles de Temps\n" + LIGNE);
    }

    // saisi et valide en nombre entier
    int saisirEtValiderNombreEntier(String message) {
        int rep;
        System.out.print(message);
        while (!clavier.hasNextInt()) {
            System.out.println(ATTENTION_ONLY_NOMBRES);
            clavier.next();//on passe la saisie actuelle
            System.out.println(message);
        }
        rep = clavier.nextInt();
        return (rep);
    }

    // determine une réponse Oui ou Non
    boolean ouiOuNon(String message) {
        boolean reponse = false;
        boolean onSorte = false;
        char OuiOuNon;
        do {
            System.out.print(message);
            OuiOuNon = clavier.next().toUpperCase().trim().charAt(0);
            if (OuiOuNon == 'O') {
                reponse = true;
                onSorte = true;
            } else if (OuiOuNon == 'N') {
                reponse = false;
                onSorte = true;
            } else {
                System.out.print(ATTENTION_CHOIX_INVALIDE);
            }
        } while (onSorte == false);
        return reponse;
    }

    // saisie et validation d'une chaine de caractères
    String saisirEtValiderChaineCaracteres(String message, String pattern) {
        String rep = "";
        System.out.print(message);
        while (!clavier.hasNext(pattern)) {
            System.out.format(ATTENTION_LETTRES);
            clavier.next();
            System.out.println(message);
        }
        rep = clavier.next();
        return (rep);
    }

    // pour chaque ligne on crée un objet <Employe> et on l'insère dans map
    void lireFeuillesTemps(String nomFichier, Map<Integer, Travailleur> map) {
        Path patFichier = Paths.get("src/" + nomFichier + ".txt");
        try (BufferedReader reader = Files.newBufferedReader(patFichier);) {
            String ligne = reader.readLine();
            while (ligne != null) {
                Travailleur Emp = new Employe(ligne);
                map.put(Emp.getMatricule(), Emp);
                ligne = reader.readLine();
            }
        } catch (IOException x) {
            System.out.println("Erreur lecture fichier");
        }
    }

    //  le parcours du bd pour afficher tous les objets Employé
    static void afficherToutesLesFeuilles(Map<Integer, Travailleur> map) {
        map.values().stream().forEach((Emp) -> {
            Emp.afficheFeuilleTemps();
        });
    }

    //  le parcours du bd pour assembler la Feuille de Temps
    static void ecrireFeuillesTemps(Map<Integer, Travailleur> map) {
        Path patFichier = Paths.get("src/" + FICHIER_SORTIE + ".txt");
        try (BufferedWriter writer = Files.newBufferedWriter(patFichier);) {
            for (Integer matricule : map.keySet()) {
                Travailleur Emp = map.get(matricule);
                writer.write(Emp.assemblerFeuilleTemps());
            }
        } catch (IOException x) {
            System.out.println("Erreur lecture fichier");
        }
    }

    // recherche dans bd et affiche la Feuille de Temps d'un Employé en particulière  
    void afficherUneFeuille(Map<Integer, Travailleur> map) {
        int matricule = saisirEtValiderNombreEntier("Entrez la Matricule de l'Employe: ");
        if (map.containsKey(matricule)) {
            System.out.println("\nVoici la Feuille de Temps pour la matricule: " + matricule);
            map.get(matricule).afficheFeuilleTemps();
        } else {
            System.out.println("Attention!, la matricule saisi ne correspond à aucun Employe");
        }
    }

    // recherche dans bd et affiche la Feuille de Temps d'un Employé en particulière  
    void validerFeuilleTemps(Map<Integer, Travailleur> map) {
        int matricule = saisirEtValiderNombreEntier("Entrez la Matricule de l'Employe: ");
        if (map.containsKey(matricule)) {
            System.out.println("\nVoici la Feuille de Temps pour la matricule: " + matricule);
            map.get(matricule).afficheFeuilleTemps();
            if (ouiOuNon("\nVoulez vous valider cette Feuille de Temps ? «Oui ou Non»: ")) {
                map.get(matricule).setStatut(StatutFeuille.ACC);
                System.out.println("\nLa Feuille de Temps a été validée");
            } else {
                map.get(matricule).setStatut(StatutFeuille.REJ);
                map.get(matricule).setCommentaire(saisirEtValiderChaineCaracteres("Raison du Rejet ?: ", PATTERN_COMMENT));
                System.out.println("\nLa Feuille de Temps a été rejetée");
            }
        } else {
            System.out.println("Attention!, la matricule saisi ne correspond à aucun Employe");
        }
    }

    // affiche le menu en boucle
    void afficherMenu(Map<Integer, Travailleur> map) {
        int choix;
        do {
            System.out.println("==========================================================");
            System.out.println("|                     MENU                               |");
            System.out.println("==========================================================");
            System.out.println("| OPTIONS:                                               |");
            System.out.println("|        1: Afficher toutes les Feuilles de Temps        |");
            System.out.println("|        2: Afficher une Feuille de Temps                |");
            System.out.println("|        3: Valider une Feuille de Temps                 |");
            System.out.println("|        4: Sauvegarder les Feuilles de Temps et quitter |");
            System.out.println("==========================================================");
            choix = saisirEtValiderNombreEntier("Sélectionnez une option du MENU: ");
            // traitement selon choix
            switch (choix) {
                case 1:
                    afficherToutesLesFeuilles(map);
                    break;
                case 2:
                    afficherUneFeuille(map);
                    break;
                case 3:
                    validerFeuilleTemps(map);
                    break;
                case 4:
                    ecrireFeuillesTemps(map);
                    System.out.println("\nLes Feuilles de Temps ont été sauvegardées");
                    System.out.print("\n");
                    System.out.println("«FIN NORMALE DU PROGRAMME»");
                    break;
            }
        } while (choix != 4);
    }
}
